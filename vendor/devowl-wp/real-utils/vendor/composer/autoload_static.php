<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4601a600ae9bb58f55ff28f3e4c53f9b
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'DevOwl\\RealUtils\\Test\\' => 22,
            'DevOwl\\RealUtils\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'DevOwl\\RealUtils\\Test\\' => 
        array (
            0 => __DIR__ . '/../..' . '/test/phpunit',
        ),
        'DevOwl\\RealUtils\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4601a600ae9bb58f55ff28f3e4c53f9b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4601a600ae9bb58f55ff28f3e4c53f9b::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
