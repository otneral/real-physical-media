import { RootStore } from ".";
import { observable, flow, action, computed } from "mobx";
import { CancellablePromise } from "mobx/lib/api/flow";
import { request } from "../utils";
import {
    RequestRestSeoStatePost,
    ParamsRestSeoStatePost,
    ResponseRestSeoStatePost,
    locationRestSeoStatePost,
    RequestRestSeoStateDelete,
    ParamsRestSeoStateDelete,
    locationRestSeoStateDelete,
    ResponseRestSeoStateDelete,
    ResponseRestSeoGet,
    RequestRestSeoGet,
    ParamsRestSeoGet,
    locationRestSeoGet,
    RequestRestSeoClearDelete,
    ResponseRestSeoClearDelete,
    ParamsRestSeoClearDelete,
    locationRestSeoClearDelete,
    RequestRestSeoClearSizeDelete,
    locationRestSeoClearSizeDelete,
    ParamsRestSeoClearSizeDelete,
    ResponseRestSeoClearSizeDelete,
    ResponseRestSeoAttachmentGet,
    RequestRestSeoAttachmentGet,
    locationRestSeoAttachmentGet,
    ParamsRestSeoAttachmentGet
} from "../wp-api";
import { SeoOriginalItem } from "../models";
import { humanFileSize } from "rml";

class SeoStore {
    @observable
    public state: boolean;

    @observable
    public count: number;

    @observable
    public busy = false;

    @observable
    public items: SeoOriginalItem[] = [];

    @observable
    public bytes = 0;

    @observable
    public sizes: string[] = [];

    @observable
    public currentAttachment = 0;

    public readonly rootStore: RootStore;

    @computed public get sizeReadable() {
        return humanFileSize(this.bytes || 0);
    }

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    fetch: (reset: boolean) => CancellablePromise<void> = flow(function* (this: SeoStore, reset = false) {
        this.busy = true;
        reset === true && (this.items = []);
        reset === true && (this.sizes = []);

        let response: ResponseRestSeoGet & ResponseRestSeoAttachmentGet;

        if (this.currentAttachment > 0) {
            response = yield request<
                RequestRestSeoAttachmentGet,
                ParamsRestSeoAttachmentGet,
                ResponseRestSeoAttachmentGet
            >({
                location: locationRestSeoAttachmentGet,
                params: {
                    skip: this.items.length,
                    id: this.currentAttachment
                }
            });
        } else {
            response = yield request<RequestRestSeoGet, ParamsRestSeoGet, ResponseRestSeoGet>({
                location: locationRestSeoGet,
                params: {
                    skip: this.items.length
                }
            });
        }
        const { cnt, items, size, sizes, state } = response;
        items.forEach((o) => this.items.push(new SeoOriginalItem({ ...o, type: "original" }, this)));
        this.state = state;
        this.sizes = sizes;
        this.count = cnt;
        this.bytes = size;
        this.busy = false;
    });

    // eslint-disable-next-line @typescript-eslint/member-ordering
    clear: (size?: string) => CancellablePromise<void> = flow(function* (this: SeoStore, size) {
        if (size) {
            yield request<RequestRestSeoClearSizeDelete, ParamsRestSeoClearSizeDelete, ResponseRestSeoClearSizeDelete>({
                location: locationRestSeoClearSizeDelete,
                params: {
                    size
                }
            });
        } else {
            yield request<RequestRestSeoClearDelete, ParamsRestSeoClearDelete, ResponseRestSeoClearDelete>({
                location: locationRestSeoClearDelete
            });
        }
        this.fetch(true);
    });

    // eslint-disable-next-line @typescript-eslint/member-ordering
    setState: (state: boolean) => CancellablePromise<void> = flow(function* (this: SeoStore, state) {
        this.items = [];
        this.busy = true;
        if (state) {
            yield request<RequestRestSeoStatePost, ParamsRestSeoStatePost, ResponseRestSeoStatePost>({
                location: locationRestSeoStatePost
            });
        } else {
            yield request<RequestRestSeoStateDelete, ParamsRestSeoStateDelete, ResponseRestSeoStateDelete>({
                location: locationRestSeoStateDelete
            });
        }
        this.fetch(true);
        this.busy = false;
    });

    // eslint-disable-next-line @typescript-eslint/member-ordering
    toggleState: () => CancellablePromise<void> = flow(function* (this: SeoStore) {
        yield this.setState(!this.state);
    });

    @action
    setCurrentAttachment(id: number) {
        this.currentAttachment = id;
    }

    resetCurrentAttachment() {
        this.setCurrentAttachment(0);
    }
}

export { SeoStore };
