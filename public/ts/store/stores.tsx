import React from "react";
import { configure } from "mobx";
import { createContextFactory } from "@devowl-wp/utils";
import { OptionStore, QueueStore } from ".";
import { SeoStore } from "./seo";

configure({
    enforceActions: "always"
});

/**
 * A collection of all available stores which gets available
 * through the custom hook useStores in your function components.
 *
 * @see https://mobx.js.org/best/store.html#combining-multiple-stores
 */
class RootStore {
    private static me: RootStore;

    public optionStore: OptionStore;

    public queueStore: QueueStore;

    public seoStore: SeoStore;

    private contextMemo: {
        StoreContext: React.Context<RootStore>;
        StoreProvider: React.FC<{}>;
        useStores: () => RootStore;
    };

    public get context() {
        return this.contextMemo
            ? this.contextMemo
            : (this.contextMemo = createContextFactory((this as unknown) as RootStore));
    }

    private constructor() {
        this.optionStore = new OptionStore(this);
        this.queueStore = new QueueStore(this);
        this.seoStore = new SeoStore(this);
    }

    public static get StoreProvider() {
        return RootStore.get.context.StoreProvider;
    }

    public static get get() {
        return RootStore.me ? RootStore.me : (RootStore.me = new RootStore());
    }
}

const useStores = () => RootStore.get.context.useStores();

export { RootStore, useStores };
