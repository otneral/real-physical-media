import { observable, runInAction, flow, action, computed } from "mobx";
import { BaseOptions } from "@devowl-wp/utils";
import { RootStore } from "./stores";
import { Handler, QueueItem } from "../models";
import { ClassProperties, request } from "../utils";
import { PausedError } from "../models/pausedError";
import { CancellablePromise } from "mobx/lib/api/flow";
import {
    RequestRestQueueAutomaticPost,
    ParamsRestQueueAutomaticPost,
    ResponseRestQueueAutomaticPost,
    locationRestQueueAutomaticPost,
    RequestRestQueueAutomaticHintDelete,
    ParamsRestQueueAutomaticHintDelete,
    locationRestQueueAutomaticHintDelete,
    ResponseRestQueueAutomaticHintDelete,
    RequestRestQueueQueueNoticeDelete,
    ParamsRestQueueQueueNoticeDelete,
    ResponseRestQueueQueueNoticeDelete,
    locationRestQueueQueueNoticeDelete
} from "../wp-api";
import { isRatable } from "@devowl-wp/real-utils";

class OptionStore extends BaseOptions {
    @observable
    public view = {
        isAutomaticInfoPanelOpen: false
    };

    // Implement "others" property in your Assets.php;
    @observable
    public others: {
        installPluginNonce: string;
        initialQueue: {
            rows: ClassProperties<QueueItem>[];
            count: number;
            estimate: string;
        };
        initialPausedError?: ClassProperties<PausedError>;
        manageOptions: string;
        seoAttachmentPage: string;
        countDown: {
            processing: number;
            pause: number;
        };
        handlers: ClassProperties<Handler>[];
        isAutomaticQueueing: boolean;
        isFirstTimeMoveHintDismissed: boolean;
        isFirstTimeQueueNoticeDismissed: boolean;
        supportsAllChildrenSql: boolean;
        showLicenseNotice: boolean;
        pluginsUrl: string;
    };

    public readonly pureSlug: ReturnType<typeof BaseOptions.getPureSlug>;

    public readonly pureSlugCamelCased: ReturnType<typeof BaseOptions.getPureSlug>;

    public readonly rootStore: RootStore;

    get isRatable() {
        return isRatable(this.slug);
    }

    @computed
    public get forcePopupOpen() {
        return (
            this.view.isAutomaticInfoPanelOpen ||
            (!this.others.isFirstTimeQueueNoticeDismissed && this.rootStore.queueStore.isHandlerActive)
        );
    }

    constructor(rootStore: RootStore) {
        super();
        this.rootStore = rootStore;
        this.pureSlug = BaseOptions.getPureSlug(process.env);
        this.pureSlugCamelCased = BaseOptions.getPureSlug(process.env, true);

        // Use the localized WP object to fill this object values.
        runInAction(() => Object.assign(this, (window as any)[this.pureSlugCamelCased]));
    }

    @action
    public setAutomaticInfoPanelOpen(state: boolean) {
        this.view.isAutomaticInfoPanelOpen = state;

        // When the automatic queueing box is been viewed the first time, dismiss it
        if (!state && !this.others.isFirstTimeMoveHintDismissed) {
            this.others.isFirstTimeMoveHintDismissed = true;

            request<
                RequestRestQueueAutomaticHintDelete,
                ParamsRestQueueAutomaticHintDelete,
                ResponseRestQueueAutomaticHintDelete
            >({
                location: locationRestQueueAutomaticHintDelete
            });
        }
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    setIsAutomaticQueueing: (state: boolean) => CancellablePromise<void> = flow(function* (this: OptionStore, state) {
        yield request<RequestRestQueueAutomaticPost, ParamsRestQueueAutomaticPost, ResponseRestQueueAutomaticPost>({
            location: locationRestQueueAutomaticPost,
            request: {
                state
            }
        });
        this.others.isAutomaticQueueing = state;
    });

    // eslint-disable-next-line @typescript-eslint/member-ordering
    dismissFirstTimeQueueNotice: () => CancellablePromise<void> = flow(function* (this: OptionStore) {
        // Already dismissed
        if (this.others.isFirstTimeQueueNoticeDismissed) {
            return;
        }

        this.others.isFirstTimeQueueNoticeDismissed = true;
        yield request<
            RequestRestQueueQueueNoticeDelete,
            ParamsRestQueueQueueNoticeDelete,
            ResponseRestQueueQueueNoticeDelete
        >({
            location: locationRestQueueQueueNoticeDelete
        });
    });
}

export { OptionStore };
