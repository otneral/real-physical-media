import { createHooks } from "@wordpress/hooks";
import { DoneItem } from "../models";
import { ResponseRestQueuePost } from "../wp-api";

/**
 * @see [[Hooks.ACTION_PHYSICAL_UPDATED]]
 */
type ActionPhysicalUpdatedParams = {
    done: DoneItem;
    urls: string[];
    newSizes: ResponseRestQueuePost["done"][0]["u"];
};

class Hooks {
    /**
     * Emitted whenever a file got physically updated.
     *
     * @see [[ActionPhysicalUpdatedParams]]
     * @event
     */
    public static ACTION_PHYSICAL_UPDATED: "physical-updated" = "physical-updated";

    private static me: Hooks;

    private handler: ReturnType<typeof createHooks>;

    private constructor() {
        this.handler = createHooks();
    }

    public static get handler() {
        return Hooks.get.handler;
    }

    public static get get() {
        return Hooks.me ? Hooks.me : (Hooks.me = new Hooks());
    }
}

export { Hooks, ActionPhysicalUpdatedParams };
