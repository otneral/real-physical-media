type ClassProperties<T> = { [K in keyof T]: T[K] };

export { ClassProperties };
