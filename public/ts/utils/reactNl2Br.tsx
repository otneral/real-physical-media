import { createElement } from "react";

const newlineRegex = /(\r\n|\r|\n|<br[ ]?\/>)/g;

/**
 * @see https://codesandbox.io/s/94k7k80jxy
 */
const reactNl2Br = (text: string) =>
    text
        .split(newlineRegex)
        .map((line, index) => (line.match(newlineRegex) ? createElement("br", { key: index }) : line));

export { reactNl2Br };
