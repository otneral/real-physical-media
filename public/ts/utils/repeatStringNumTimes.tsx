function repeatStringNumTimes(string: string, times: number): string {
    if (times < 0) return "";
    return times === 1 ? string : string + repeatStringNumTimes(string, times - 1);
}

export { repeatStringNumTimes };
