import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestQueueItemPost: RouteLocationInterface = {
    path: "/queue/item/:id",
    method: RouteHttpVerb.POST
};

export type RequestRestQueueItemPost = RouteRequestInterface;

export interface ParamsRestQueueItemPost extends RouteParamsInterface {
    id: number;
}

export type ResponseRestQueueItemPost = RouteResponseInterface;
