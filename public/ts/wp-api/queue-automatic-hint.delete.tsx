import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestQueueAutomaticHintDelete: RouteLocationInterface = {
    path: "/queue/automatic-hint/dismiss",
    method: RouteHttpVerb.DELETE
};

export type RequestRestQueueAutomaticHintDelete = RouteRequestInterface;

export type ParamsRestQueueAutomaticHintDelete = RouteParamsInterface;

export type ResponseRestQueueAutomaticHintDelete = RouteResponseInterface;
