import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestQueueQueueNoticeDelete: RouteLocationInterface = {
    path: "/queue/notice/dismiss",
    method: RouteHttpVerb.DELETE
};

export type RequestRestQueueQueueNoticeDelete = RouteRequestInterface;

export type ParamsRestQueueQueueNoticeDelete = RouteParamsInterface;

export type ResponseRestQueueQueueNoticeDelete = RouteResponseInterface;
