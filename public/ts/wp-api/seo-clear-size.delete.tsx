import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoClearSizeDelete: RouteLocationInterface = {
    path: "/seo/clear/size/:size",
    method: RouteHttpVerb.DELETE
};

export type RequestRestSeoClearSizeDelete = RouteRequestInterface;

export interface ParamsRestSeoClearSizeDelete extends RouteParamsInterface {
    size: string;
}

export type ResponseRestSeoClearSizeDelete = RouteResponseInterface;
