import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoStateDelete: RouteLocationInterface = {
    path: "/seo/state",
    method: RouteHttpVerb.DELETE
};

export type RequestRestSeoStateDelete = RouteRequestInterface;

export type ParamsRestSeoStateDelete = RouteParamsInterface;

export type ResponseRestSeoStateDelete = RouteResponseInterface;
