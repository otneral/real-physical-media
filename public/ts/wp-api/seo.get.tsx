import { RouteLocationInterface, RouteHttpVerb, RouteRequestInterface, RouteParamsInterface } from "@devowl-wp/utils";
import { ResponseRestSeoAttachmentGet } from ".";

export const locationRestSeoGet: RouteLocationInterface = {
    path: "/seo",
    method: RouteHttpVerb.GET
};

export type RequestRestSeoGet = RouteRequestInterface;

export interface ParamsRestSeoGet extends RouteParamsInterface {
    skip: number;
}

export type ResponseRestSeoGet = ResponseRestSeoAttachmentGet;
