import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoClearAttachmentDelete: RouteLocationInterface = {
    path: "/seo/clear/attachment/:id",
    method: RouteHttpVerb.DELETE
};

export type RequestRestSeoClearAttachmentDelete = RouteRequestInterface;

export interface ParamsRestSeoClearAttachmentDelete extends RouteParamsInterface {
    id: number;
}

export type ResponseRestSeoClearAttachmentDelete = RouteResponseInterface;
