import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestCustomFieldGet: RouteLocationInterface = {
    path: "/customField/:id",
    method: RouteHttpVerb.GET
};

export type RequestRestCustomFieldGet = RouteRequestInterface;

export interface ParamsRestCustomFieldGet extends RouteParamsInterface {
    id: number;
}

export interface ResponseRestCustomFieldGet extends RouteResponseInterface {
    html: string;
}
