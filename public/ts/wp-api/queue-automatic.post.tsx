import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestQueueAutomaticPost: RouteLocationInterface = {
    path: "/queue/automatic",
    method: RouteHttpVerb.POST
};

export interface RequestRestQueueAutomaticPost extends RouteRequestInterface {
    state: boolean;
}

export type ParamsRestQueueAutomaticPost = RouteParamsInterface;

export type ResponseRestQueueAutomaticPost = RouteResponseInterface;
