import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestNoticeLicenseDelete: RouteLocationInterface = {
    path: "/notice/license",
    method: RouteHttpVerb.DELETE
};

export type RequestRestNoticeLicenseDelete = RouteRequestInterface;

export type ParamsRestNoticeLicenseDelete = RouteParamsInterface;

export type ResponseRestNoticeLicenseDelete = RouteResponseInterface;
