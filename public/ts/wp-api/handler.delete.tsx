import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestHandlerDelete: RouteLocationInterface = {
    path: "/handler",
    method: RouteHttpVerb.DELETE
};

export interface RequestRestHandlerDelete extends RouteRequestInterface {
    handler: string;
}

export type ParamsRestHandlerDelete = RouteParamsInterface;

export type ResponseRestHandlerDelete = RouteResponseInterface;
