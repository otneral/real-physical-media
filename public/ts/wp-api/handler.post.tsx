import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestHandlerPost: RouteLocationInterface = {
    path: "/handler",
    method: RouteHttpVerb.POST
};

export interface RequestRestHandlerPost extends RouteRequestInterface {
    handler: string;
}

export type ParamsRestHandlerPost = RouteParamsInterface;

export type ResponseRestHandlerPost = RouteResponseInterface;
