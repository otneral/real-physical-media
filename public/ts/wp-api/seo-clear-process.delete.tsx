import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoClearProcessDelete: RouteLocationInterface = {
    path: "/seo/clear/process/:processId",
    method: RouteHttpVerb.DELETE
};

export type RequestRestSeoClearProcessDelete = RouteRequestInterface;

export interface ParamsRestSeoClearProcessDelete extends RouteParamsInterface {
    processId: string;
}

export type ResponseRestSeoClearProcessDelete = RouteResponseInterface;
