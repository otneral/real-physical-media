import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";
import { ClassProperties } from "../utils";
import { QueueItem } from "../models";

export const locationRestQueueGet: RouteLocationInterface = {
    path: "/queue",
    method: RouteHttpVerb.GET
};

export type RequestRestQueueGet = RouteRequestInterface;

export type ParamsRestQueueGet = RouteParamsInterface;

export interface ResponseRestQueueGet extends RouteResponseInterface {
    rows: ClassProperties<QueueItem>[];
    count: number;
    estimate: string;
}
