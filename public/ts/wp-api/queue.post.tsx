import { RouteLocationInterface, RouteHttpVerb, RouteRequestInterface, RouteParamsInterface } from "@devowl-wp/utils";
import { ResponseRestQueueGet } from ".";

export const locationRestQueuePost: RouteLocationInterface = {
    path: "/queue",
    method: RouteHttpVerb.POST
};

export interface RequestRestQueuePost extends RouteRequestInterface {
    skip: "retry" | "skip" | "default";
}

export type ParamsRestQueuePost = RouteParamsInterface;

export interface ResponseRestQueuePost extends ResponseRestQueueGet {
    done: [
        {
            d: string;
            f: string;
            i: boolean;
            u: { [key: string]: string };
            fid: number;
            ms: number;
        }
    ];
    doneUrlPrefix: string;
}
