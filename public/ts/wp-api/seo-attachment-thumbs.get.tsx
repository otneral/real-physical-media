import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";
import { SeoItem } from "../models";
import { ClassProperties } from "../utils";

export const locationRestSeoAttachmentThumbsGet: RouteLocationInterface = {
    path: "/seo/:processId/thumbs",
    method: RouteHttpVerb.GET
};

export type RequestRestSeoAttachmentThumbsGet = RouteRequestInterface;

export interface ParamsRestSeoAttachmentThumbsGet extends RouteParamsInterface {
    processId: string;
}

export interface ResponseRestSeoAttachmentThumbsGet extends RouteResponseInterface {
    items: ClassProperties<SeoItem>[];
}
