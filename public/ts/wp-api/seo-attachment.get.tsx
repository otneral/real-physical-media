import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";
import { SeoOriginalItem } from "../models";
import { ClassProperties } from "../utils";

export const locationRestSeoAttachmentGet: RouteLocationInterface = {
    path: "/seo/:id",
    method: RouteHttpVerb.GET
};

export type RequestRestSeoAttachmentGet = RouteRequestInterface;

export interface ParamsRestSeoAttachmentGet extends RouteParamsInterface {
    skip: number;
    id: number;
}

export interface ResponseRestSeoAttachmentGet extends RouteResponseInterface {
    cnt: number;
    items: ClassProperties<SeoOriginalItem>[];
    size: number;
    sizes: string[];
    state: boolean;
}
