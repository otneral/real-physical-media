import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoStatePost: RouteLocationInterface = {
    path: "/seo/state",
    method: RouteHttpVerb.POST
};

export type RequestRestSeoStatePost = RouteRequestInterface;

export type ParamsRestSeoStatePost = RouteParamsInterface;

export type ResponseRestSeoStatePost = RouteResponseInterface;
