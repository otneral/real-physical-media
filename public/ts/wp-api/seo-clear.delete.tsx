import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoClearDelete: RouteLocationInterface = {
    path: "/seo/clear/all",
    method: RouteHttpVerb.DELETE
};

export type RequestRestSeoClearDelete = RouteRequestInterface;

export type ParamsRestSeoClearDelete = RouteParamsInterface;

export type ResponseRestSeoClearDelete = RouteResponseInterface;
