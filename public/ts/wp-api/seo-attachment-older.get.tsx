import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";
import { SeoItem } from "../models";
import { ClassProperties } from "../utils";

export const locationRestSeoAttachmentOlderGet: RouteLocationInterface = {
    path: "/seo/:id/older",
    method: RouteHttpVerb.GET
};

export type RequestRestSeoAttachmentOlderGet = RouteRequestInterface;

export interface ParamsRestSeoAttachmentOlderGet extends RouteParamsInterface {
    id: number;
}

export interface ResponseRestSeoAttachmentOlderGet extends RouteResponseInterface {
    items: ClassProperties<SeoItem>[];
}
