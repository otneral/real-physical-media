import {
    RouteLocationInterface,
    RouteHttpVerb,
    RouteRequestInterface,
    RouteParamsInterface,
    RouteResponseInterface
} from "@devowl-wp/utils";

export const locationRestSeoClearThumbnailDelete: RouteLocationInterface = {
    path: "/seo/clear/id/:id",
    method: RouteHttpVerb.DELETE
};

export type RequestRestSeoClearThumbnailDelete = RouteRequestInterface;

export interface ParamsRestSeoClearThumbnailDelete extends RouteParamsInterface {
    id: number;
}

export type ResponseRestSeoClearThumbnailDelete = RouteResponseInterface;
