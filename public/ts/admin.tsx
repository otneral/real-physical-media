/* istanbul ignore file: we do not need to care about the entry point file as errors are detected through integration tests (E2E) */

/**
 * The entry point for the admin side wp-admin resource.
 */
import "@devowl-wp/utils"; // Import once for startup polyfilling (e. g. setimmediate)
import { render } from "react-dom";
import { RootStore } from "./store";
import "./style/antd.less";
import "./style/admin.scss";
import { DialogButton, HandlerPicker } from "./components";
import {
    applyOptionRestButtons,
    applyManualClickHandler,
    applyDeactivateYearMonthOption,
    applyUrlReplace,
    applyCustomField,
    applyShowNoHandlerNotice,
    applyOptionCronjobUrlInput,
    applyOptionChangeNotice
} from "./others";
import { ConfigProvider } from "antd";
import { handleCorrupRestApi } from "@devowl-wp/utils";
import { request } from "./utils";

handleCorrupRestApi?.({
    [RootStore.get.optionStore.restNamespace]: async () => {
        await request({
            location: {
                path: "/plugin"
            }
        });
    }
});

const adminBar = document.getElementById("admin-bar-rpm");
adminBar &&
    render(
        <ConfigProvider prefixCls={process.env.ANTD_PREFIX}>
            <RootStore.StoreProvider>
                <DialogButton />
            </RootStore.StoreProvider>
        </ConfigProvider>,
        adminBar
    );

const handlerPicker = document.getElementById("rpm-handlers");
handlerPicker &&
    render(
        <ConfigProvider prefixCls={process.env.ANTD_PREFIX}>
            <RootStore.StoreProvider>
                <HandlerPicker />
            </RootStore.StoreProvider>
        </ConfigProvider>,
        handlerPicker
    );

applyOptionRestButtons();
applyOptionCronjobUrlInput();
applyManualClickHandler();
applyDeactivateYearMonthOption();
applyUrlReplace();
applyCustomField();
applyShowNoHandlerNotice();
applyOptionChangeNotice();

// Expose this functionalities to add-ons, but you need to activate the library functionality
// in your webpack configuration, see also https://webpack.js.org/guides/author-libraries/
export * from "@devowl-wp/utils";
export * from "./wp-api";
export * from "./store";
export * from "./utils";
