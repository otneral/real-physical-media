/* eslint-disable prefer-destructuring */
import { FC } from "react";
import { SeoItem, SeoOriginalItem } from "../models";
import { observer, Observer } from "mobx-react";
import { useStores } from "../store";
import { DeleteOutlined } from "@ant-design/icons";
import { _n, __ } from "../utils";

const SeoEntryRow: FC<{ item: SeoOriginalItem | SeoItem }> = observer(({ item }) => {
    const {
        optionStore: {
            others: { seoAttachmentPage, manageOptions }
        }
    } = useStores();

    let thumbnails: SeoOriginalItem["thumbnails"];
    let olderEntries: SeoOriginalItem["olderEntries"];
    let thumbnail: SeoOriginalItem["thumbnail"];
    let thumbs: SeoOriginalItem["thumbs"];
    let older: SeoOriginalItem["older"];
    let fetchedThumbnails: SeoOriginalItem["fetchedThumbnails"];
    let fetchedOlder: SeoOriginalItem["fetchedOlder"];
    let fetchThumbnails: SeoOriginalItem["fetchThumbnails"];
    let fetchOlder: SeoOriginalItem["fetchOlder"];
    let busy: SeoOriginalItem["busy"];

    if (item instanceof SeoOriginalItem) {
        thumbnails = item.thumbnails;
        olderEntries = item.olderEntries;
        thumbnail = item.thumbnail;
        thumbs = item.thumbs;
        older = item.older;
        fetchedThumbnails = item.fetchedThumbnails;
        fetchedOlder = item.fetchedOlder;
        fetchThumbnails = item.fetchThumbnails.bind(item);
        fetchOlder = item.fetchOlder.bind(item);
        busy = item.busy;
    }

    const { id, attachment, fromUrl, fromUrlShort, toUrl, toUrlShort, modified, deleteBusy, size } = item;

    return (
        <>
            <tr key={id}>
                {!!thumbnail && (
                    <td>
                        <img src={thumbnail} width="50px" height="auto" />
                    </td>
                )}
                <td colSpan={thumbnail ? 1 : 2}>
                    <a href={fromUrl} target="_blank" rel="noopener noreferrer" style={{ wordBreak: "break-all" }}>
                        {fromUrlShort}
                    </a>
                    {!fetchedThumbnails && thumbs > 0 && (
                        <a
                            style={{ textDecoration: "underline", cursor: "pointer", display: "block" }}
                            onClick={fetchThumbnails}
                        >
                            {_n("+ one thumbnail", "+ %d thumbnails", thumbs, thumbs)}
                        </a>
                    )}
                    {!fetchedOlder && older > 0 && (
                        <a
                            style={{ textDecoration: "underline", cursor: "pointer", display: "block" }}
                            onClick={fetchOlder}
                        >
                            {_n("+ one older redirect", "+ %d older redirects", older, older)}
                        </a>
                    )}
                    {busy && <span className="spinner is-active"></span>}
                </td>
                <td>
                    <a href={toUrl} target="_blank" rel="noopener noreferrer" style={{ wordBreak: "break-all" }}>
                        {toUrlShort}
                    </a>
                </td>
                <td>
                    <a href={seoAttachmentPage + attachment} target="_blank" rel="noopener noreferrer">
                        # {attachment}
                    </a>
                </td>
                <td>
                    {modified}
                    {size !== "full" && ` / ${size}`}
                </td>
                <td>
                    {!!manageOptions &&
                        (!deleteBusy ? (
                            <DeleteOutlined onClick={() => item.trash()} />
                        ) : (
                            <span className="spinner is-active"></span>
                        ))}
                </td>
            </tr>
            {!!thumbnails?.length && (
                <>
                    <tr>
                        <th colSpan={6}>{__("Thumbnails")}</th>
                    </tr>
                    {thumbnails.map((thumbnail) => (
                        <Observer key={thumbnail.id}>{() => <SeoEntryRow item={thumbnail} />}</Observer>
                    ))}
                </>
            )}
            {!!olderEntries?.length && (
                <>
                    <tr>
                        <th colSpan={6}>{__("Older redirects")}</th>
                    </tr>
                    {olderEntries.map((older) => (
                        <Observer key={older.id}>{() => <SeoEntryRow item={older} />}</Observer>
                    ))}
                </>
            )}
        </>
    );
});

export { SeoEntryRow };
