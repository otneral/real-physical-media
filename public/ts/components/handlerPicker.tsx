import { FC } from "react";
import { observer } from "mobx-react";
import { Spin, Alert, message } from "antd";
import { CheckCircleFilled, PlayCircleFilled } from "@ant-design/icons";
import { __ } from "../utils";
import { useStores } from "../store";
import $ from "jquery";

const HandlerPicker: FC<{}> = observer(() => {
    const { handlers } = useStores().queueStore;

    return (
        <>
            {handlers.map((h) => (
                <li className="rpm-renamer" key={h.id}>
                    <Spin spinning={h.busy}>
                        <h4>
                            {h.isHandler && <CheckCircleFilled style={{ color: "green" }} />} {h.name}
                        </h4>
                        <p>
                            {__("Author")}: {h.author}
                            <br />
                            {h.origin}
                        </p>
                        {!!h.error && <Alert type="error" showIcon message={<span>{h.error}</span>} />}
                        {!h.error &&
                            ((!h.isActivated && (
                                <a href="#" onClick={h.installAndActivate.bind(h)}>
                                    <PlayCircleFilled style={{ color: "#333" }} />{" "}
                                    {h.isInstalled ? __("Activate plugin") : __("Install for free")}
                                </a>
                            )) || (
                                <a
                                    href="#"
                                    onClick={(e) => {
                                        h.toggle()
                                            .catch((e) => {
                                                message.error(e.responseJSON.message);
                                            })
                                            .then(() => {
                                                // In settings page reload because settings are visible depending on handler state
                                                if ($("body").hasClass("options-media-php")) {
                                                    window.location.reload();
                                                }
                                            });
                                        e.preventDefault();
                                        return false;
                                    }}
                                    style={{ display: "block" }}
                                >
                                    {h.isHandler ? __("Deactivate as handler") : __("Activate as handler")}
                                </a>
                            ))}
                    </Spin>
                </li>
            ))}
        </>
    );
});

export { HandlerPicker };
