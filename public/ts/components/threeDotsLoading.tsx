import { FC, useState, useEffect } from "react";
import { repeatStringNumTimes } from "../utils";

const ThreeDotsLoading: FC<{ loading: boolean }> = ({ loading }) => {
    const [dots, setDots] = useState(0);

    useEffect(() => {
        const interval = setInterval(() => setDots(dots === 3 ? 1 : dots + 1), 1000);
        return () => clearInterval(interval);
    }, []);

    if (!loading) {
        return null;
    }

    return (
        <span style={{ fontSize: "monospace", width: "10px", display: "inline-block" }}>
            {repeatStringNumTimes(".", dots)}
        </span>
    );
};

export { ThreeDotsLoading };
