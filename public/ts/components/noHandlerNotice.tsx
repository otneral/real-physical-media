import { FC } from "react";
import { observer } from "mobx-react";
import { __, _i } from "../utils";
import { HandlerPicker } from "./handlerPicker";

const NoHandlerNotice: FC<{}> = observer(() => {
    return (
        <>
            <p>
                {_i(
                    __(
                        "You have successfully activated {{strong}}Real Physical Media{{/strong}}. To use Real Physical Media you need to install a media file handler, which moves files in your media library later. We recommend Media File Renamer (free)."
                    ),
                    {
                        strong: <strong />
                    }
                )}
            </p>
            <ul>
                <HandlerPicker />
            </ul>
        </>
    );
});

export { NoHandlerNotice };
