import { FC } from "react";
import { useStores } from "../store";
import { ThreeDotsLoading } from ".";
import { WarningOutlined, LoadingOutlined, PullRequestOutlined } from "@ant-design/icons";
import { observer } from "mobx-react";

const StatusCounter: FC<{}> = observer(() => {
    const { queueStore } = useStores();

    return (
        <span>
            {queueStore.pausedError ? (
                <span>
                    <WarningOutlined /> {queueStore.count}
                </span>
            ) : (
                <span>
                    {queueStore.countdown <= 0 ? <LoadingOutlined spin /> : <PullRequestOutlined />} {queueStore.count}{" "}
                    <ThreeDotsLoading loading={queueStore.count > 0} />
                </span>
            )}
        </span>
    );
});

export { StatusCounter };
