import { FC, useCallback } from "react";
import { useStores } from "../store";
import { __, _n } from "../utils";
import {
    LoadingOutlined,
    SwapRightOutlined,
    FolderOutlined,
    CheckCircleFilled,
    PauseCircleOutlined
} from "@ant-design/icons";
import { observer } from "mobx-react";
import { Alert } from "antd";

/**
 * Show the queue list with estimated time and done items.
 */
const QueueList: FC<{}> = observer(() => {
    const { queueStore, optionStore } = useStores();
    const { count, countdown, queue, isHandlerActive, done, doneUnique, estimate } = queueStore;
    const { manageOptions, isFirstTimeQueueNoticeDismissed } = optionStore.others;
    const handleDismissQueueNotice = useCallback(() => {
        optionStore.dismissFirstTimeQueueNotice();
    }, [optionStore]);

    return (
        <ul>
            <li className="rpm-headline">{__("Queue")}</li>
            {!isFirstTimeQueueNoticeDismissed && isHandlerActive && (
                <li>
                    <Alert
                        message={
                            <span>
                                {__(
                                    "You have successfully installed and configured Real Phyiscal Media! In this window you can follow live, which files were physically moved recently."
                                )}{" "}
                                <a href="#" onClick={handleDismissQueueNotice}>
                                    {__("Got it!")}
                                </a>
                            </span>
                        }
                        type="warning"
                        showIcon
                        style={{ marginBottom: "10px" }}
                    />
                </li>
            )}
            {count > 0 && (
                <li style={{ textAlign: "center" }}>
                    {_n("One file left... (~ %s)", "%d files left... (~ %s)", count, count, estimate)}
                    <br />
                    {countdown > 0 ? (
                        <span>
                            {_n("Resume processing now...", "Resume processing in %d seconds...", count, count)}
                        </span>
                    ) : (
                        __("Loading...")
                    )}
                    {!!manageOptions && (
                        <div>
                            {__("Do not close this admin page.")}{" "}
                            <a href={`${manageOptions}#rml-rpm_cronjob`}>{__("Learn more about cronjob services")}</a>
                        </div>
                    )}
                </li>
            )}
            {queue.map(({ filename, destinationPath, loaded, total, attachment }, i) => (
                <li className="rpm-wrap" key={attachment}>
                    {i === 0 && (countdown <= 0 ? <LoadingOutlined spin /> : <PauseCircleOutlined />)}
                    {i === 0 && <span> {((loaded / total) * 100).toFixed(0)}% </span>}
                    <strong>{filename}</strong>
                    <div>
                        <SwapRightOutlined /> <FolderOutlined /> {destinationPath}
                    </div>
                </li>
            ))}
            {count === 0 && (
                <li style={{ textAlign: "center" }}>
                    {__("No entries")}
                    <br />
                    {isHandlerActive &&
                        (countdown > 0 ? (
                            <span>
                                {_n(
                                    "Check for new entries soon...",
                                    "Check for new entries in %d seconds...",
                                    countdown,
                                    countdown
                                )}
                            </span>
                        ) : (
                            __("Loading...")
                        ))}
                    <br />
                </li>
            )}
            {done.length > 0 && (
                <li className="rpm-headline">
                    {__("Last processed items %s", done.length > 3 ? __("(%d total)", done.length) : "")}
                </li>
            )}
            {doneUnique.map(({ filename, destinationPath, /*duration,*/ preview }) => (
                <li className="rpm-wrap" key={`${filename}${destinationPath}`}>
                    {!!preview && (
                        <img src={preview} width="auto" height="35px" style={{ float: "left", marginRight: "10px" }} />
                    )}
                    <CheckCircleFilled style={{ color: "green" }} /> <strong>{filename}</strong>
                    {/* ({Math.round(duration)} ms) */}
                    <div>
                        <SwapRightOutlined /> <FolderOutlined /> {destinationPath}
                    </div>
                </li>
            ))}
        </ul>
    );
});

export { QueueList };
