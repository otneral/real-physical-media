import { FC, useState, useMemo, useCallback } from "react";
import { SeoDialog } from ".";
import { Alert, message } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { useStores } from "../store";
import { __, request, reactNl2Br } from "../utils";
import { observer } from "mobx-react";
import { locationRestNoticeLicenseDelete } from "../admin";
import $ from "jquery";

/**
 * List the available handlers and show a button to install / activate them.
 */
const PopupSection: FC<{}> = observer(() => {
    const { optionStore, seoStore, queueStore } = useStores();
    const {
        view: { isAutomaticInfoPanelOpen },
        others: {
            manageOptions,
            isAutomaticQueueing,
            showLicenseNotice,
            pluginsUrl,
            supportsAllChildrenSql,
            isFirstTimeQueueNoticeDismissed
        }
    } = optionStore;
    const { isHandlerActive } = queueStore;

    const [isAutomaticBusy, setAutomaticBusy] = useState(false);
    const [isSeoModalOpen, setSeoModalOpen] = useState(false);
    const isWpAdmin = useMemo(() => $("body").hasClass("wp-admin"), []);

    /**
     * When the link is clicked and automatic queuing is activate, let
     * the user confirm and deactivate it. If not activated yet, show the automatic box.
     */
    const handleSetAutomaticInfoPanelOpen = useCallback(async () => {
        if (isAutomaticInfoPanelOpen) {
            optionStore.setAutomaticInfoPanelOpen(false);
        } else if (isAutomaticQueueing && window.confirm(__("Are you sure?"))) {
            setAutomaticBusy(true);
            try {
                await optionStore.setIsAutomaticQueueing(false);
            } catch (e) {
                message.error(e.responseJSON.message);
            }
            setAutomaticBusy(false);
        } else {
            optionStore.setAutomaticInfoPanelOpen(true);
        }
    }, [isAutomaticInfoPanelOpen, isAutomaticQueueing, setAutomaticBusy, optionStore]);

    /**
     * Activate automatic queueing.
     */
    const handleAutomaticActivate = useCallback(async () => {
        setAutomaticBusy(true);
        optionStore.setAutomaticInfoPanelOpen(false);
        try {
            await optionStore.setIsAutomaticQueueing(true);
        } catch (e) {
            message.error(e.responseJSON.message);
        }
        setAutomaticBusy(false);
    }, [setAutomaticBusy, setAutomaticBusy, optionStore]);

    /**
     * Dismiss the license notice for a given time (transient).
     */
    const handleDismissLicenseNotice = useCallback(async () => {
        await request({ location: locationRestNoticeLicenseDelete });
        window.location.reload();
    }, []);

    const handleSeoModalOpen = useCallback(() => {
        setSeoModalOpen(true);
        seoStore.fetch(true);
        $(".rpm-popover").addClass("ant-popover-hidden");
    }, [setSeoModalOpen, seoStore]);

    const handleSeoModalClose = useCallback(() => setSeoModalOpen(false), [setSeoModalOpen]);

    return (
        <>
            <SeoDialog open={isSeoModalOpen} onClose={handleSeoModalClose} />
            <ul>
                <li className="rpm-headline">{__("Real Physical Media")}</li>
                {!isAutomaticInfoPanelOpen ? (
                    <>
                        {!!manageOptions && (
                            <li>
                                <a
                                    style={{ textDecoration: "underline", cursor: "pointer" }}
                                    onClick={handleSetAutomaticInfoPanelOpen}
                                >
                                    {isAutomaticBusy && <LoadingOutlined spin />}
                                    {isAutomaticQueueing
                                        ? __("Deactivate automatic queueing")
                                        : __("Activate automatic change detection")}
                                </a>
                                &nbsp;&middot;&nbsp;
                                {isWpAdmin && (
                                    <span>
                                        <a
                                            style={{ textDecoration: "underline", cursor: "pointer" }}
                                            onClick={handleSeoModalOpen}
                                        >
                                            {__("SEO Redirects")}
                                        </a>
                                        &nbsp;&middot;&nbsp;
                                    </span>
                                )}
                                <a href={`${manageOptions}#rml-rpm-handlers`}>{__("Settings")}</a>
                            </li>
                        )}
                        {showLicenseNotice && isFirstTimeQueueNoticeDismissed && isHandlerActive && (
                            <li>
                                <Alert
                                    message={
                                        <span>
                                            {__("Product license not yet activated.")}{" "}
                                            <a href={pluginsUrl}>{__("Activate license")}</a> &middot;{" "}
                                            <a href="#" onClick={handleDismissLicenseNotice}>
                                                {__("Dismiss notice")}
                                            </a>
                                        </span>
                                    }
                                    type="info"
                                    style={{ marginBottom: "10px" }}
                                />
                            </li>
                        )}
                    </>
                ) : (
                    <li className="rpm-automatic-box">
                        <strong>{__("Activate automatic change detection")}</strong>
                        {supportsAllChildrenSql ? (
                            <span>
                                <p>
                                    {reactNl2Br(
                                        __(
                                            'Real Physical Media can automatically detect when you have moved a file to the Real Media Library and can also move it physically in your file system.\n\nBefore you enable this feature, go to the "Attachment Details" or "Edit Media" dialog of the file you last moved and click the "Move physically" button. Then check that the file is available at the new URL as expected.\n\nWe recommend that you perform this manual check before you enable automatic moving, because special WordPress configurations rarely result in errors with the Media File Renamer handler.'
                                        )
                                    )}
                                </p>
                                <br />
                                <a className="button button-primary" onClick={handleAutomaticActivate}>
                                    {__("Activate")}
                                </a>
                                <br />
                            </span>
                        ) : (
                            <p>
                                {__(
                                    "Unfortunately, you cannot activate automatic change detection. Your current database system does not support some mandatory functions. Please update your MySQL/MariaDB database to the latest version. Technical reason: Your database system does not allow you to read from a hierarchical query created by wp_rml_create_all_children_sql()."
                                )}
                            </p>
                        )}
                        <br />
                        <a
                            style={{ textDecoration: "underline", cursor: "pointer" }}
                            onClick={handleSetAutomaticInfoPanelOpen}
                        >
                            {__("Back")}
                        </a>
                    </li>
                )}
            </ul>
        </>
    );
});

export { PopupSection };
