import { FC } from "react";
import { observer } from "mobx-react";
import { useStores } from "../store";
import { __ } from "../utils";
import { SeoEntryRow } from ".";

const SeoEntriesTable: FC<{}> = observer(() => {
    const { seoStore } = useStores();
    const { count, items, busy } = seoStore;

    return (
        <table className="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th scope="col" style={{ width: "55px" }}></th>
                    <th scope="col" className="column-primary">
                        {__("Old url")}
                    </th>
                    <th scope="col">{__("Redirect to")}</th>
                    <th scope="col" style={{ width: "50px" }}>
                        {__("File")}
                    </th>
                    <th scope="col" style={{ width: "130px" }}>
                        {__("Modified / Size name")}
                    </th>
                    <th scope="col" style={{ width: "20px" }}></th>
                </tr>
            </thead>
            <tbody id="the-list">
                {count > 0 && items.map((obj) => <SeoEntryRow key={obj.id} item={obj} />)}
                {count === 0 && (
                    <tr>
                        <td colSpan={6}>{__("No entries")}</td>
                    </tr>
                )}
            </tbody>
            <tfoot>
                <tr>
                    <th colSpan={6}>
                        {busy ? (
                            <span className="spinner is-active"></span>
                        ) : items.length < count ? (
                            <a className="button" onClick={() => seoStore.fetch(false)}>
                                {__("Load more")}
                            </a>
                        ) : (
                            <span>{__("No more redirects available")}</span>
                        )}
                    </th>
                </tr>
            </tfoot>
        </table>
    );
});

export { SeoEntriesTable };
