import { FC } from "react";
import { useStores } from "../store";
import { Alert } from "antd";
import { __ } from "../utils";
import { FileOutlined, RollbackOutlined, StepForwardOutlined, UpOutlined, RightOutlined } from "@ant-design/icons";
import { observer } from "mobx-react";

const PausedError: FC<{}> = observer(() => {
    const { pausedError } = useStores().queueStore;

    return (
        <ul>
            <li className="rpm-headline">{__("Error")}</li>
            <li>
                <Alert type="error" showIcon message={pausedError.message} />
            </li>
            <li>
                <a
                    href={pausedError.data.attachmentUrl}
                    style={{ textDecoration: "none", cursor: "pointer" }}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <FileOutlined /> {__("Open file")}
                </a>
            </li>
            <li>
                <a onClick={pausedError.retry} style={{ textDecoration: "none", cursor: "pointer" }}>
                    <RollbackOutlined /> {__("Retry")}
                </a>
            </li>
            <li>
                <a onClick={pausedError.skip} style={{ textDecoration: "none", cursor: "pointer" }}>
                    <StepForwardOutlined /> {__("Skip file and continue")}
                </a>
            </li>
            <li>
                <a onClick={pausedError.toggleUi} style={{ textDecoration: "none", cursor: "pointer" }}>
                    {pausedError.uiDetails ? <UpOutlined /> : <RightOutlined />} {__("Details")}
                </a>
            </li>
            {pausedError.uiDetails && (
                <li>
                    <textarea
                        style={{ width: "100%" }}
                        rows={12}
                        readOnly
                        value={`${__("Code")}: ${pausedError.data.code}\n
${__("Message")}: ${pausedError.data.message}\n\n
${pausedError.data.stack}`}
                    />
                </li>
            )}
        </ul>
    );
});

export { PausedError };
