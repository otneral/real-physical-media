import { FC, useCallback } from "react";
import { StatusCounter, PausedError, PopupSection, QueueList } from ".";
import { useStores } from "../store";
import { __ } from "../utils";
import $ from "jquery";
import { observer } from "mobx-react";
import { RatingPointer } from "@devowl-wp/real-utils";
import { CloseOutlined } from "@ant-design/icons";
import { Tooltip } from "antd";

const DialogButton: FC<{}> = observer(() => {
    const { optionStore, queueStore } = useStores();
    const { isRatable, slug, forcePopupOpen } = optionStore;
    const { pausedError, done } = queueStore;

    const handleCloseIcon = useCallback(() => {
        $(".rpm-popover").addClass("ant-popover-hidden");
        optionStore.dismissFirstTimeQueueNotice();
        optionStore.setAutomaticInfoPanelOpen(false);
    }, [optionStore]);

    // Show rating popup
    isRatable &&
        done.length &&
        new RatingPointer(
            slug,
            $("#admin-bar-rpm").parent(),
            __(
                "<strong>Real Physical Media</strong> just moved a file physically. Do you like it? Do us a favor and rate us."
            ),
            "rpm-real-utils-bar-pointer"
        );

    const tooltipAttr = {} as React.ComponentProps<typeof Tooltip>;
    if (forcePopupOpen) {
        tooltipAttr.visible = true;
    }

    return (
        <span data-done={done.length}>
            <Tooltip
                trigger="hover"
                prefixCls="ant-popover"
                mouseEnterDelay={0.1}
                mouseLeaveDelay={0.1}
                overlayClassName="rpm-popover"
                placement="bottomLeft"
                overlay={
                    <span>
                        <CloseOutlined onClick={handleCloseIcon} />
                        <ul>
                            <li className="rpm-section">{pausedError ? <PausedError /> : <QueueList />}</li>
                            <li className="rpm-section">
                                <PopupSection />
                            </li>
                        </ul>
                    </span>
                }
                {...tooltipAttr}
            >
                <span>
                    <StatusCounter />
                </span>
            </Tooltip>
        </span>
    );
});

export { DialogButton };
