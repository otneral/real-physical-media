import { FC, useCallback } from "react";
import { useStores } from "../store";
import { __ } from "../utils";
import { SeoDialog } from ".";
import { useState } from "react";

const SeoAttachmentLink: FC<{
    id: number;
    onOpen?: () => void;
    onClose?: () => void;
}> = ({ id, onOpen, onClose }) => {
    const [isOpen, setIsOpen] = useState(false);
    const { seoStore } = useStores();

    const handleOpen = useCallback(() => {
        setIsOpen(true);
        seoStore.setCurrentAttachment(id);
        seoStore.fetch(true);
        onOpen?.();
    }, [setIsOpen, id, seoStore, onOpen]);

    const handleClose = useCallback(() => {
        setIsOpen(false);
        seoStore.resetCurrentAttachment();
        onClose?.();
    }, [setIsOpen, seoStore, onClose]);

    return (
        <>
            <a key="link" className="button" onClick={handleOpen}>
                {__("SEO Redirects")}
            </a>
            <SeoDialog key="dialog" open={isOpen} onClose={handleClose} />
        </>
    );
};

export { SeoAttachmentLink };
