import { observer } from "mobx-react";
import { FC } from "react";
import Modal from "react-responsive-modal";
import { SeoEntriesTable } from ".";
import { Dropdown, Menu } from "antd";
import { __ } from "../utils";
import { useStores } from "../store";
import { DownOutlined } from "@ant-design/icons";

const SeoDialog: FC<{ open?: boolean; onClose?: () => void }> = observer(({ open, onClose }) => {
    const { optionStore, seoStore } = useStores();
    const { manageOptions } = optionStore.others;
    const { currentAttachment, count, state, sizes, sizeReadable } = seoStore;

    return (
        <Modal open={open} onClose={onClose} center styles={{ overlay: { zIndex: 10000 } }}>
            <h1 className="wp-heading-inline">{__("SEO Redirects")}</h1>
            <hr className="wp-header-end" />
            {currentAttachment === 0 && !!manageOptions && (
                <ul className="subsubsub" style={{ marginBottom: "10px" }}>
                    <li className="all">
                        {__("All")} <span className="count">({count})</span> |
                    </li>
                    <li className="all">
                        <a
                            style={{ textDecoration: "underline", cursor: "pointer" }}
                            onClick={seoStore.toggleState.bind(seoStore)}
                        >
                            {state ? __("Deactivate all redirects") : __("Activate all redirects")}
                        </a>{" "}
                        |
                    </li>
                    <li className="all">
                        <Dropdown
                            transitionName={null}
                            overlayClassName="rpm-clear-overlay"
                            overlay={
                                <Menu
                                    onClick={({ key }) =>
                                        window.confirm(__("Are you sure?")) &&
                                        seoStore.clear(key === "ALL" ? undefined : key)
                                    }
                                >
                                    {sizes.map((size) => (
                                        <Menu.Item key={size}>{__("Clear redirects for size '%s'", size)}</Menu.Item>
                                    ))}
                                    <Menu.Item key="ALL">{__("Clear all redirects")}</Menu.Item>
                                </Menu>
                            }
                        >
                            <a className="ant-dropdown-link" href="#">
                                {__("Clear")} <DownOutlined />
                            </a>
                        </Dropdown>
                    </li>
                    <li className="all" style={{ display: "none" }}>
                        &nbsp;{__("SEO database size")}: {sizeReadable}
                    </li>
                </ul>
            )}
            <div id="posts-filter">
                <SeoEntriesTable />
            </div>
            <br className="clear" />
        </Modal>
    );
});

export { SeoDialog };
