import $ from "jquery";
import { message } from "antd";
import { __ } from "../utils";

function applyOptionCronjobUrlInput() {
    $(document).on("click", "input.rpm-cronjob-url", function (this: JQuery<HTMLInputElement>) {
        $(this).get(0).select();
        document.execCommand("copy");
        message.success(__("Cronjob URL successfully copied to the clipboard."));
    });
}

export { applyOptionCronjobUrlInput };
