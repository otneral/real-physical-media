import { render } from "react-dom";
import { NoHandlerNotice } from "../components";
import { RootStore } from "../store";
import $ from "jquery";
import { when } from "mobx";
import { ConfigProvider } from "antd";

function applyShowNoHandlerNotice() {
    const notice = document.getElementById("rpm-no-handler-notice");
    if (notice) {
        render(
            <ConfigProvider prefixCls={process.env.ANTD_PREFIX}>
                <RootStore.StoreProvider>
                    <NoHandlerNotice />
                </RootStore.StoreProvider>
            </ConfigProvider>,
            notice
        );

        // Show notice
        $(notice).removeClass("hidden");

        // Hide notice when handler active
        const dispatcher = when(
            () => RootStore.get.queueStore.isHandlerActive,
            () => {
                setTimeout(() => $(notice).remove(), 1000);
                dispatcher();
            }
        );
    }
}

export { applyShowNoHandlerNotice };
