import { __ } from "../utils";

function applyDeactivateYearMonthOption() {
    const el = document.getElementById("uploads_use_yearmonth_folders");
    el &&
        ((el as any).disabled = true) &&
        el.after(
            (() => {
                const redText = document.createElement("span");
                redText.innerHTML = __(
                    "This option is not available because you are using <strong>Real Physical Media Plugin</strong> to physically restructure your files.<br/>"
                );
                return redText;
            })()
        );
}

export { applyDeactivateYearMonthOption };
