import $ from "jquery";

/**
 * If a `.rpm-change-option` notice is found, the previous input change should trigger the
 * visibility.
 */
function applyOptionChangeNotice() {
    $(".rpm-option-change").each(function () {
        const $notice = $(this);
        $notice
            .parents("td")
            .find(":input")
            .on("change input", () => $notice.removeClass("hidden"));
    });
}

export { applyOptionChangeNotice };
