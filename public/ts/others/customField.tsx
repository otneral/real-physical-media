import { hooks } from "rml";
import { request } from "../utils";
import {
    RequestRestCustomFieldGet,
    ParamsRestCustomFieldGet,
    ResponseRestCustomFieldGet,
    locationRestCustomFieldGet
} from "../admin";
import { render } from "react-dom";
import $ from "jquery";
import { RootStore } from "../store";
import { SeoAttachmentLink } from "../components";

function applyCustomField() {
    hooks.register("wprfc/rpm/customField", async function (this: JQuery, { id }: { id: number }) {
        $(this).html('<span class="spinner is-active"></span>');
        const { html } = await request<RequestRestCustomFieldGet, ParamsRestCustomFieldGet, ResponseRestCustomFieldGet>(
            {
                location: locationRestCustomFieldGet,
                params: {
                    id
                }
            }
        );
        $(this).html(html);

        // SEO single media dialog
        const singleSeoContainer = $(this).find(".rpm-single-seo").get(0);
        singleSeoContainer &&
            render(
                <RootStore.StoreProvider>
                    <SeoAttachmentLink
                        id={id}
                        onOpen={() => $(".media-modal.wp-core-ui").parent().hide()}
                        onClose={() => $(".media-modal.wp-core-ui").parent().show()}
                    />
                </RootStore.StoreProvider>,
                singleSeoContainer
            );
    });
}

export { applyCustomField };
