import $ from "jquery";
import { message } from "antd";
import { RootStore } from "../store";
import { __, Hooks, ActionPhysicalUpdatedParams } from "../utils";

/**
 * Add item to queue manually when clicking the manual button.
 */
function applyManualClickHandler() {
    $(document).on("click", "a.rpm-manual-queue", function (e) {
        const $o = $(this);
        const id = +$o.attr("data-id");
        (async () => {
            const hide = message.loading(__("Add file to queue..."));
            try {
                await RootStore.get.queueStore.addItem(id);

                // Update compat view
                $o.parents("[data-wprfc]").attr("data-wprfc-visible", 1);
            } catch (e) {
                message.error(e.responseJSON.message);
            } finally {
                hide();
            }
        })();
        return e.preventDefault();
    });

    // Update compat view also when queue item processed
    Hooks.handler.addAction(
        Hooks.ACTION_PHYSICAL_UPDATED,
        "internal/compatView",
        ({ done: { attachment } }: ActionPhysicalUpdatedParams) => {
            $(`[data-wprfc="rpm/customField"][data-id="${attachment}"]:visible`).attr("data-wprfc-visible", 1);
        }
    );
}

export { applyManualClickHandler };
