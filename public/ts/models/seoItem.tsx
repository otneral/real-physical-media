import { observable, set, runInAction, computed, flow } from "mobx";
import { ClassProperties, request } from "../utils";
import { SeoStore } from "../store";
import { SeoOriginalItem } from "./seoOriginalItem";
import { CancellablePromise } from "mobx/lib/api/flow";
import {
    RequestRestSeoClearAttachmentDelete,
    ParamsRestSeoClearAttachmentDelete,
    ResponseRestSeoClearAttachmentDelete,
    locationRestSeoClearAttachmentDelete,
    RequestRestSeoClearThumbnailDelete,
    ParamsRestSeoClearThumbnailDelete,
    ResponseRestSeoClearThumbnailDelete,
    locationRestSeoClearThumbnailDelete,
    ParamsRestSeoClearProcessDelete,
    ResponseRestSeoClearProcessDelete,
    locationRestSeoClearProcessDelete,
    RequestRestSeoClearProcessDelete
} from "../wp-api";

class SeoItem {
    @observable
    public type?: "older" | "thumbnail" | "original" = "original";

    @observable
    public original?: SeoOriginalItem;

    @observable
    public id: number;

    @observable
    public processId: string;

    @observable
    public attachment: number;

    @observable
    public fromHash: string;

    @observable
    public fromUrl: string;

    @observable
    public toUrl: string;

    @observable
    public modified: string;

    @observable
    public size: string;

    @observable
    public validFullHash: string;

    @observable
    public deleteBusy = false;

    @computed get fromUrlShort() {
        return this.urlTruncate(this.fromUrl);
    }

    @computed get toUrlShort() {
        return this.urlTruncate(this.toUrl);
    }

    public readonly seoStore: SeoStore;

    constructor(seoItem: Partial<ClassProperties<SeoItem>>, seoStore: SeoStore) {
        this.seoStore = seoStore;
        runInAction(() => set(this, seoItem));
    }

    private urlTruncate(url: string) {
        const { origin } = window.location;
        return url.startsWith(origin) ? url.substr(origin.length) : url;
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    trash: () => CancellablePromise<boolean> = flow(function* (this: SeoOriginalItem) {
        this.deleteBusy = true;

        try {
            switch (this.type) {
                case "original":
                    yield request<
                        RequestRestSeoClearAttachmentDelete,
                        ParamsRestSeoClearAttachmentDelete,
                        ResponseRestSeoClearAttachmentDelete
                    >({
                        location: locationRestSeoClearAttachmentDelete,
                        params: {
                            id: this.attachment
                        }
                    });
                    this.seoStore.items.splice(this.seoStore.items.indexOf(this), 1);
                    this.seoStore.count--;
                    break;
                case "thumbnail":
                    yield request<
                        RequestRestSeoClearThumbnailDelete,
                        ParamsRestSeoClearThumbnailDelete,
                        ResponseRestSeoClearThumbnailDelete
                    >({
                        location: locationRestSeoClearThumbnailDelete,
                        params: {
                            id: this.id
                        }
                    });
                    this.original.thumbnails.splice(this.original.thumbnails.indexOf(this), 1);
                    break;
                case "older":
                    yield request<
                        RequestRestSeoClearProcessDelete,
                        ParamsRestSeoClearProcessDelete,
                        ResponseRestSeoClearProcessDelete
                    >({
                        location: locationRestSeoClearProcessDelete,
                        params: {
                            processId: this.processId
                        }
                    });
                    this.original.olderEntries.splice(this.original.olderEntries.indexOf(this), 1);
                    break;
                default:
                    return false;
            }
            return true;
        } catch (e) {
            return false;
        } finally {
            this.deleteBusy = false;
        }
    });
}

export { SeoItem };
