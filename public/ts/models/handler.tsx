import { observable, set, runInAction, flow } from "mobx";
import { ClassProperties, request } from "../utils";
import { CancellablePromise } from "mobx/lib/api/flow";
import {
    RequestRestHandlerDelete,
    ParamsRestHandlerDelete,
    ResponseRestHandlerDelete,
    locationRestHandlerDelete,
    RequestRestHandlerPost,
    ParamsRestHandlerPost,
    ResponseRestHandlerPost,
    locationRestHandlerPost
} from "../wp-api";
import { QueueStore } from "../store";
import wp from "wp";
import $ from "jquery";

class Handler {
    @observable
    public id: string;

    @observable
    public file: string;

    @observable
    public name: string;

    @observable
    public error: string;

    @observable
    public author: string;

    @observable
    public origin: string;

    @observable
    public installUrl?: string;

    @observable
    public isActivated: boolean;

    @observable
    public isInstalled: boolean;

    @observable
    public isHandler: boolean;

    @observable
    public activatePluginUrl: string;

    @observable
    public busy = false;

    public readonly queueStore: QueueStore;

    constructor(handler: ClassProperties<Handler>, queueStore: QueueStore) {
        this.queueStore = queueStore;
        runInAction(() => set(this, handler));
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    toggle: () => CancellablePromise<void> = flow(function* (this: Handler) {
        this.busy = true;
        try {
            if (this.isHandler) {
                yield request<RequestRestHandlerDelete, ParamsRestHandlerDelete, ResponseRestHandlerDelete>({
                    location: locationRestHandlerDelete,
                    request: {
                        handler: this.id
                    }
                });
            } else {
                yield request<RequestRestHandlerPost, ParamsRestHandlerPost, ResponseRestHandlerPost>({
                    location: locationRestHandlerPost,
                    request: {
                        handler: this.id
                    }
                });
            }

            // Deactivate others and activate me
            !this.isHandler && this.queueStore.handlers.forEach((o) => o.isHandler && (o.isHandler = false));
            this.isHandler = !this.isHandler;
        } finally {
            this.busy = false;
        }
    });

    // eslint-disable-next-line @typescript-eslint/member-ordering
    installAndActivate: () => CancellablePromise<void> = flow(function* (this: Handler) {
        this.busy = true;
        const ajaxSend: (options: any) => Promise<void> = wp?.ajax?.send; // wp-utils.js is not loaded on frontend, so it is optional

        // Try to install plugin via AJAX request
        if (this.isInstalled) {
            // Immediate activate the plugin
            try {
                yield $.get(this.activatePluginUrl).promise();
                this.isActivated = true;

                // Immediate activate the plugin as rename handler
                yield this.toggle();
            } catch (e) {
                // Fallback to usual redirect
                window.location.href = this.activatePluginUrl;
            }
        } else {
            if (ajaxSend) {
                try {
                    yield ajaxSend({
                        data: {
                            action: "install-plugin",
                            slug: this.id,
                            // eslint-disable-next-line @typescript-eslint/naming-convention
                            _ajax_nonce: this.queueStore.rootStore.optionStore.others.installPluginNonce
                        }
                    });

                    this.isInstalled = true;

                    yield this.installAndActivate();
                } catch (e) {
                    // Fallback to install url (this can happen if FTP is configured to install plugin)
                    window.location.href = this.installUrl;
                }
            } else {
                window.location.href = this.installUrl;
            }
        }
    });
}

export { Handler };
