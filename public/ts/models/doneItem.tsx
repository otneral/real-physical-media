import { observable, set, runInAction } from "mobx";
import { ClassProperties } from "../utils";

class DoneItem {
    @observable
    public attachment: number;

    @observable
    public isImage = false;

    @observable
    public currentFolderId: number;

    @observable
    public filename: string;

    @observable
    public destinationPath: string;

    @observable
    public preview = "";

    @observable
    public duration = 0;

    constructor(doneItem: ClassProperties<DoneItem>) {
        runInAction(() => set(this, doneItem));
    }
}

export { DoneItem };
