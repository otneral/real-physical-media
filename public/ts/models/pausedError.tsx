import { observable, set, runInAction, action } from "mobx";
import { ClassProperties } from "../utils";
import { QueueStore } from "../store";

class PausedError {
    @observable
    public code: string;

    @observable
    public data: any;

    @observable
    public message: string;

    @observable
    public uiDetails = false;

    public readonly queueStore: QueueStore;

    constructor(pausedError: ClassProperties<PausedError>, queueStore: QueueStore) {
        this.queueStore = queueStore;
        runInAction(() => set(this, pausedError));
    }

    @action
    public toggleUi() {
        this.uiDetails = !this.uiDetails;
    }

    public retry() {
        this.queueStore.setForceNextProcessMethod("retry").refresh();
    }

    public skip() {
        this.queueStore.setForceNextProcessMethod("skip").refresh();
    }
}

export { PausedError };
