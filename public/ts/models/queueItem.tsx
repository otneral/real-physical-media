import { observable, set, runInAction } from "mobx";
import { ClassProperties } from "../utils";

class QueueItem {
    @observable
    public id: number;

    @observable
    public attachment: number;

    @observable
    public loaded: number;

    @observable
    public total: number;

    @observable
    public created: number;

    @observable
    public logId?: number;

    @observable
    public cleanupPath?: string;

    @observable
    public currentFolderId: number;

    @observable
    public filename: string;

    @observable
    public sourceAbsPath: string;

    @observable
    public sourcePath: string;

    @observable
    public destinationPath: string;

    @observable
    public destinationPathRelativeUploadDir: string;

    constructor(queueItem: ClassProperties<QueueItem>) {
        runInAction(() => set(this, queueItem));
    }
}

export { QueueItem };
