import { SeoItem } from "./seoItem";
import { ClassProperties, request } from "../utils";
import { observable, flow } from "mobx";
import { CancellablePromise } from "mobx/lib/api/flow";
import {
    RequestRestSeoAttachmentThumbsGet,
    ParamsRestSeoAttachmentThumbsGet,
    ResponseRestSeoAttachmentThumbsGet,
    locationRestSeoAttachmentThumbsGet,
    ResponseRestSeoAttachmentOlderGet,
    ParamsRestSeoAttachmentOlderGet,
    RequestRestSeoAttachmentOlderGet,
    locationRestSeoAttachmentOlderGet
} from "../wp-api";
import { SeoStore } from "../store";

class SeoOriginalItem extends SeoItem {
    @observable
    public thumbnail = "";

    @observable
    public thumbs = 0;

    @observable
    public older = 0;

    @observable
    public fetchedThumbnails = false;

    @observable
    public fetchedOlder = false;

    @observable
    public thumbnails: SeoItem[] = [];

    @observable
    public olderEntries: SeoOriginalItem[] = [];

    @observable
    public busy = false;

    constructor(seoOriginalItem: Partial<ClassProperties<SeoOriginalItem>>, seoStore: SeoStore) {
        super(seoOriginalItem, seoStore);
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    fetchThumbnails: () => CancellablePromise<void> = flow(function* (this: SeoOriginalItem) {
        this.fetchedThumbnails = true;
        this.busy = true;
        this.thumbnails = [];

        const { items } = (yield request<
            RequestRestSeoAttachmentThumbsGet,
            ParamsRestSeoAttachmentThumbsGet,
            ResponseRestSeoAttachmentThumbsGet
        >({
            location: locationRestSeoAttachmentThumbsGet,
            params: {
                processId: this.processId
            }
        })) as ResponseRestSeoAttachmentThumbsGet;

        this.thumbnails = items.map((i) => new SeoItem({ ...i, type: "thumbnail", original: this }, this.seoStore));
        this.busy = false;
    });

    // eslint-disable-next-line @typescript-eslint/member-ordering
    fetchOlder: () => CancellablePromise<void> = flow(function* (this: SeoOriginalItem) {
        this.fetchedThumbnails = true;
        this.busy = true;
        this.thumbnails = [];

        const { items } = (yield request<
            RequestRestSeoAttachmentOlderGet,
            ParamsRestSeoAttachmentOlderGet,
            ResponseRestSeoAttachmentOlderGet
        >({
            location: locationRestSeoAttachmentOlderGet,
            params: {
                id: this.attachment
            }
        })) as ResponseRestSeoAttachmentOlderGet;

        this.olderEntries = items.map(
            (i) => new SeoOriginalItem({ ...i, type: "older", original: this }, this.seoStore)
        );
        this.busy = false;
    });
}

export { SeoOriginalItem };
